import requests
from datetime import *
from twilio.rest import Client

STOCK = "TSLA"
COMPANY_NAME = "Tesla Inc"

is_five = False

# API twilio
account_sid = 'AC475c8aeed5ab08998da9e1ecb246fdd7'
auth_token = '1c00f83a5432aa4b8fc11d506701bc26'

# API stock
api_alpha_vantage = 'WS8W90ZWJZ1QLPOW'
url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={STOCK}&interval=5min&apikey={api_alpha_vantage}'
response_stock = requests.get(url=url)
response_stock.raise_for_status()


json_data = response_stock.json()
daily_data = json_data['Time Series (Daily)']

today = datetime.today().strftime('%Y-%m-%d')

# Creating two lists, filling data_list with day date and close_list with 'close' price of stock
data_list = []
close_list = []
for x, y in daily_data.items():
    data_list.append(x)
    close_list.append(y['4. close'])

# Creating a list combining previous (date & 'close' price stock) information
close_data = [{'date': k, 'close': v} for k, v in zip(data_list, close_list)]

day1 = float(close_data[0]['close'])
day2 = float(close_data[1]['close'])

calculation = day1 - day2

percentage = round((calculation * 100) / day1)
if abs(percentage) >= 5:
    is_five = True
else:
    pass


# API news
news_api = 'ab2203fe6a1b404fb66541e16b8d8a62'
url = (f'https://newsapi.org/v2/everything?'
       'q=TSLA&'
       f'from={close_data[0]["date"]}&'
       f'to={close_data[1]["date"]}&'
       f'apiKey={news_api}')
response_news = requests.get(url)
response_news.raise_for_status()

article_1 = response_news.json()['articles'][:3][0]
article_2 = response_news.json()['articles'][:3][1]
article_3 = response_news.json()['articles'][:3][2]


# Sending three articles via SMS
if is_five:
    # Article 1
    if percentage > 0 and percentage >= 5:
        client = Client(account_sid, auth_token)
        message = client.messages \
            .create(
            body=f'''TSLA: 🔺{percentage}%.\nHeadline: {article_1['title']}.\nBrief: {article_1['description']}.''',
            from_='+12708195570',
            to='+380689838414'
        )
    elif 0 > percentage <= -5:
        client = Client(account_sid, auth_token)
        message = client.messages \
            .create(
            body=f'''TSLA: 🔻{abs(percentage)}%.\nHeadline: {article_1['title']}.\nBrief: {article_1['description']}.''',
            from_='+12708195570',
            to='+380689838414'
        )

    # Article 2
    if percentage > 0 and percentage >= 5:
        client = Client(account_sid, auth_token)
        message = client.messages \
            .create(
            body=f'''TSLA: 🔺{percentage}%.\nHeadline: {article_2['title']}.\nBrief: {article_2['description']}.''',
            from_='+12708195570',
            to='+380689838414'
        )
    elif 0 > percentage <= 55:
        client = Client(account_sid, auth_token)
        message = client.messages \
            .create(
            body=f'''TSLA: 🔻{abs(percentage)}%.\nHeadline: {article_2['title']}.\nBrief: {article_2['description']}.''',
            from_='+12708195570',
            to='+380689838414'
        )

    # Article 3
    if percentage > 0 and percentage >= 5:
        client = Client(account_sid, auth_token)
        message = client.messages \
            .create(
            body=f'''TSLA: 🔺{percentage}%.\nHeadline: {article_3['title']}.\nBrief: {article_3['description']}.''',
            from_='+12708195570',
            to='+380689838414'
        )
    elif 0 > percentage <= -5:
        client = Client(account_sid, auth_token)
        message = client.messages \
            .create(
            body=f'''TSLA: 🔻{abs(percentage)}%.\nHeadline: {article_3['title']}.\nBrief: {article_3['description']}.''',
            from_='+12708195570',
            to='+380689838414'
        )


